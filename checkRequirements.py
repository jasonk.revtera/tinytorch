"""Test availability of required packages.
Run it like python3 checkRequirements.py {some_dir_with_requirements.txt}
"""

import sys
import os.path as PP
if not (len(sys.argv) > 1 and PP.isdir(sys.argv[1]) and PP.isfile(PP.join(sys.argv[1], 'requirements.txt'))):
  raise AttributeError('supply a directory that contains a requirements.txt')

import unittest
from pathlib import Path

from pip._internal.req.req_file import parse_requirements
from pip._internal.req.constructors import install_req_from_parsed_requirement
from pip._internal.network.session import PipSession
from pip._internal.exceptions import DistributionNotFound



_REQUIREMENTS_PATH = Path(sys.argv[1], "requirements.txt")


class TestRequirements(unittest.TestCase):
  """Test availability of required packages."""

  def test_requirements(self):
    """Test that each required package is available."""
    # Ref: https://stackoverflow.com/a/45474387/
    session = PipSession()
    self.errors = []
    print(f'checking {_REQUIREMENTS_PATH}')
    requirements = parse_requirements(str(_REQUIREMENTS_PATH), session)
    for requirement in requirements:
      req_to_install = install_req_from_parsed_requirement(requirement)
      req_to_install.check_if_exists(use_user_site=False)
      try:
        with self.subTest(req_to_install=str(req_to_install)):
          if not req_to_install.satisfied_by:
            raise DistributionNotFound(f'missing {req_to_install}')
      except DistributionNotFound as dnf:
        self.errors.append(str(req_to_install))
        print(dnf)
      except Exception as e:
        raise(e)
if __name__ == '__main__':
  T = TestRequirements()
  T.test_requirements()
  if len(T.errors) == 0:
    print(f'all requirement in {_REQUIREMENTS_PATH} are satisfied')
  else:
    print(f'missing {len(T.errors)}. {_REQUIREMENTS_PATH} not satisfied')
