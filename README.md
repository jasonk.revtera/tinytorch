# TinyTorch

  This is a small (near-drop-in) replacement for pytorch.Tensor class
  that is designed for easy study. I started to write this, simply to
  understand how some of pytorch's capabilities are actually
  implemented. This package is inspired by (and uses parts of) Andrej
  Karpathy's  [micrograd](https://github.com/karpathy/micrograd) package. Think of
  this package as micrograd with a simplified implementation of
  a Tensor class that support `backward()` just like `pytorch.Tensor`

  This package is intended as a demonstration of some of the cool internal
  details of pytorch, and how some of these cool features are implemented.

  For reducing code size, I split up the data access portions of Tensor
  into TinyArray. TinyArray implements most of the data layout and access logic
  (i.e. similar to (but way simpler than))  numpy arrays and 'pytorch.Tensor'

  TinyTensor implements some of torch.Tensor access logic. In almost all cases,
  I took pains to showcase the simplest implementation that mimimcs
  pytorch's behavior. Many obvious optimizations were avoided for the sake
  of code simplicity.

  While this package *CAN* be used to design and test neural networks,
  it is not meant to do so. (Just use pytorch!) This package is purely
  for education purposes, meant to teach how some of the clever techniques
  employed by pytorch could be implemented.

  ` jason.w.kim @ protonmail.com `


## `tinytorch/MyStuff.py`
  Few utlities, including a DebugHelper class which is designed to showcase
  a pattern for how to implement a helper class that can be used to keep track
  of which flags (for debug out) are enabled, etc...

  ```
  DebugPush(...)
    pushes a set of flags to activate debugging facility.
    flags are typically things like
    CLASS   - i.e. debug only this particular class
    CLASS.METHODNAME - debug this particular method in the class CLASS
    INSTANCE -  (debug only this instance)
    INSTANCE.METHOD - (debug only this instance's method)
    anything else - you can custom craft Debug flags. i.e. strings, sets, dicts, ...


  DebugPop()
    pops the topmost set of flags. Does not empty the stack

   ```
## `tinytorch/tensorhelpers.py`
  Helper routines for pretty printing.
  ```
  vprt(*args, **kwargs) - vertically align each argument
  hprt(*args, **kwargs) - horizontally align each arguement
  ```
  `hprt` and `vprt` can be combined to present an "ascii-art" picture
  of its arguments.
  For example, I find it useful to horizontally paste the name of a Tensor,
  then its shape, followed by its actual elements.

## view.py
  Utility to draw DOT diagrams of expression trees in both Value and Tensor
