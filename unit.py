#! /usr/bin/python3
from tinytorch import *
import torch
import math

import traceback
# L = [3,4,5]
# S = getStrideOf(L)
# O = [0] *len(S) + [0]

# print(f'shape={L} stride={S} {O=}')A
# print(list(map(lambda x: x[0] * x[1], zip(S+[1], O))))

Failures = []
TotalChecks = 0
# set this to true to make checkAndPrint rethrow the error
# useful if this test is to be run inside unittest or some such
CAP_TraditionalAssert = False
CAP_DbgFlag = 'print'

def checkAndPrintI(header, fmt, eqtxt = None, DbgFlag=None, TraditionalAssert=None, **kwargs):
  global Failures
  global TotalChecks
  global CAP_TraditionalAssert
  global CAP_DbgFlag
  if TraditionalAssert is not None:
    TraditionalAssert = CAP_TraditionalAssert
  if DbgFlag is not None:
    DbgFlag = CAP_DbgFlag
  fmt = str(fmt)
  if header is not None:
    header = str(header)
    if len(header) > 0:
      header += ' '
  suffix = ''
  prefix = ''
  if eqtxt is not None:
    eqtxt = str(eqtxt)
    header = str(header)
    TotalChecks += 1
    if fmt != eqtxt:
      suffix = f' != {eqtxt}'
      prefix = 'FAILED '
      try:
        assert fmt == eqtxt, prefix+header+fmt+suffix
      except Exception as e:
        M = [ f'CHECK FAILED {header} ||{fmt} ==? {eqtxt}||\n' ] + traceback.format_stack()[:-1]
        Failures.append(''.join(M))
        if TraditionalAssert:
          raise e
        # Otherwise, just consume the assert and
        # print out the failures at the end as part of one final assert
  DbgPrt(DbgFlag, prefix + header + fmt + suffix, **kwargs)

def checkAndPrint(fmt, eqtxt = None, DbgFlag='print', TraditionalAssert=None, **kwargs):
  checkAndPrintI('', fmt, eqtxt, DbgFlag, TraditionalAssert, **kwargs)

def checkAndPrint2(header, fmt, eqtxt = None, DbgFlag='print', TraditionalAssert=None, **kwargs):
  checkAndPrintI(header, fmt, eqtxt, DbgFlag, TraditionalAssert, **kwargs)

########################################################################
# BEGIN test definitions
########################################################################
  
def TestHelpers():
  fff=torch.eye(3,3,dtype=torch.int32)
  w,h,R = getdim('hello')
  assert(w==5 and h == 1 and len(R)==h)
  w,h,R = getdim("BBEEE\nCC\nDDDD")
  assert(w == 5 and h == 3 and len(R) == h)
  print(hprt('hello', aA="BBEEE\nCC\nDDDD", foo=fff, world=1))
  print(vprt('hello', world=fff, next=hprt('a','b','c','d', e='\n\n00000\n0')))
  print(Shapes('LAST', world=fff, N=123456))
  K = (1,2,3)
  print(isinstance(K, Sequence), isinstance(K, list))
  print(Shapes(A=fff, B=fff, C=fff))


def TestArray():
  # DebugPush('print', Array, Array.view, Array.Iter, Array.permute, Array.__setitem__)
  # FF = set(['print'])
  # print(CurrDebugFlags(), FF, CurrDebugFlags()&FF == FF, IsDebugging(FF), IsDebugging('print'), findDebugPrinter())
  # DebugPop()
  # DebugPop()

  A = Array([i for i in range(18)])
  B = A.view(3,  -1)
  C = A.view(-1, 3, 3)
  D = A.view(3,-1, 3)
  E = A.view(3,3, -1)
  checkAndPrint(B.to_string(multiline=True).split('\n'),
                ["[[0 1 2 3 4 5]"," [6 7 8 9 10 11]"," [12 13 14 15 16 17]]"])

  checkAndPrint(getStrideOf(4,2,6), [12, 6, 1])
  checkAndPrint(getStrideOf(4), [1])
  checkAndPrint(getStrideOf([4,2,6]), [12,6,1])
  checkAndPrint(getOffsetOf([12,6,1], [1,1,5]), 23)
  checkAndPrint(getShapeOf([0,1,2,3,4]), [5])
  A = Array([0,1,2,3,4])

  checkAndPrint(f'shape={A.shape}, stride={A.stride}, offset={A.offset}',
                'shape=[5], stride=[1], offset=[0, 0]')
  # DebugPush(Array, Array.__init__, Array.getitemHelper)
  A0 = [
    [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14]],
    [[15,16,17,18,19],[20,21,22,23,24],[25,26,27,28,29]]
    ]
  A = Array(A0)
  checkAndPrint(A.repr, 'shape:[2, 3, 5],stride:[15, 5, 1],offset:[0, 0, 0, 0]')
  B = A[0]
  checkAndPrint(B.repr, 'shape:[3, 5],stride:[5, 1],offset:[0, 0, 0]')
  B = A[1]
  checkAndPrint(B.repr, 'shape:[3, 5],stride:[5, 1],offset:[0, 0, 15]')

  C = [0,0,0]
  S = [2,3,4]
  Checks = [([0, 0, 1], 2),([0, 0, 2], 2),
            ([0, 0, 3], 2),([0, 1, 0], 1),
            ([0, 1, 1], 2),([0, 1, 2], 2),
            ([0, 1, 3], 2),([0, 2, 0], 1),
            ([0, 2, 1], 2),([0, 2, 2], 2),
            ([0, 2, 3], 2),([1, 0, 0], 0),
            ([1, 0, 1], 2),([1, 0, 2], 2),
            ([1, 0, 3], 2),([1, 1, 0], 1),
            ([1, 1, 1], 2),([1, 1, 2], 2),
            ([1, 1, 3], 2),([1, 2, 0], 1),
            ([1, 2, 1], 2),([1, 2, 2], 2),
            ([1, 2, 3], 2),([0, 0, 0], -1),
            ([0, 0, 1], 2),([0, 0, 2], 2),
            ([0, 0, 3], 2),([0, 1, 0], 1),
            ([0, 1, 1], 2),([0, 1, 2], 2),
            ([0, 1, 3], 2),([0, 2, 0], 1)]
  for check in Checks:
    checkAndPrint(incrementCoord(S, C), check)
  checkAndPrint(f'{A.item=}', 'A.item=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]')

  test_list = [1, [1], [12, 'test', set([3, 4, 5])], 2, 3, ('hello', 'world'), [range(3)]]

  checkAndPrint(f'{list(flattenArray(test_list))=}', "list(flattenArray(test_list))=[1, 1, 12, 'test', 3, 4, 5, 2, 3, 'hello', 'world', 0, 1, 2]")
  checkAndPrint(f'{list(flattenArray(A0))=}')

  ZeroD = Array([7], [1,1])
  checkAndPrint(ZeroD.data, [ [ 7 ] ])
  checkAndPrint(ZeroD.shape, [1, 1]),
  checkAndPrint(ZeroD.stride, [1, 1]),
  checkAndPrint(ZeroD.offset, [0, 0, 0])
  Z2 = ZeroD.simplify()
  checkAndPrint(Z2, 7)
  checkAndPrint(Z2.shape, []),
  checkAndPrint(Z2.stride, []),
  checkAndPrint(Z2.offset, [0])

  # #checkAndPrint(list(flattenArray(A0)))
  # checkAndPrint(f'{A.item=}')
  A.item[6] = -7

  checkAndPrint(f'{B.item=}', 'B.item=[0, 1, 2, 3, 4, 5, -7, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]')
  # DebugPop()
  A0 = [
    [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14]],
    [[15,16,17,18,19],[20,21,22,23,24],[25,26,27,28,29]],
    [[30,31,32,33,34],[35,36,37,38,39],[40,41,42,43,44]],
    ]
  # AB = [ float(i)/2 for i in range(45) ]
  # AC = Array(AB, shape=[3,3,5])
  # # checkAndPrint(f'{AC[2]=}')
  # checkAndPrint(AA.shape, list(BB.shape))
  # x,y,z = 0,0,0

  # DebugPush('print', Array, Array.getitemHelper, 'matmul', unFlattenArray, 'unFlattenArray2')
  # x = 1
  # checkAndPrint(hprt(Checking='X', x=x,y=y,z=z))
  # aa = AA[x,:,:]
  # bb = BB[x,:,:]
  # print(hprt(aa=aa, bb=bb))
  # DebugPush('print', Array, Array.getitemHelper, 'matmul', unFlattenArray, 'unFlattenArray2', getOffsetOf)
  # aal = aa.data
  # bbl = bb.tolist()
  # #   DebugPop()
  # checkAndPrint(aal, bbl)
  def test3DSlice(Arr):
    AA = Array(Arr)
    BB = torch.tensor(Arr)
    test3DSlice2(AA, BB)

    numElts = MUL(*AA.shape, 1,1)
    # get every divisor of numElts
    divisors = list(filter(lambda l: numElts % l == 0, range(2, numElts//2+1)))
    ## because we are reducing a 3d array into 2d, the view for [ -1, d[i] ] is the same as [ numElts//d[i], -1 ]
    views = [ [-1, i] for i in divisors ]
    # print(f'{numElts=} {AA.shape, AA.stride, AA.offset} {divisors=} {views=} ')
    simplifies = [ [1] + v for v in views ] + [ v + [1] for v in views ] + \
      [ [v[0], 1, v[1]] for v in views ]
    # print(f'{simplifies=}')
    # DebugPush('print')
    for view in views:
      av = AA.view(*view)
      bv = BB.view(*view)
      checkAndPrint(av.data, bv.tolist())
      checkAndPrint(f'trying view of {view} {AA.shape, AA.stride, AA.offset} resultied in {av.shape, av.stride, av.offset}')
    for view in simplifies:
      av = AA.view(*view)
      bv = BB.view(*view)
      checkAndPrint(av.data, bv.tolist())
      #DebugPush('print')
      checkAndPrint(f'trying view of {view} {AA.shape, AA.stride, AA.offset} resultied in {av.shape, av.stride, av.offset}')
      #DebugPop()
    permute = [[0,1,2], [0,2,1],
               [1,0,2], [1,2,0],
               [2,0,1], [2,1,0]]
    #DebugPush('print', Array, Array.permute)
    for perm in permute:
      aa = AA.permute(*perm)
      bb = BB.permute(*perm)
      checkAndPrint(f'permuting {AA.shape} to {perm} [{aa.shape=} {aa.stride=} {aa.offset=}]')
      checkAndPrint(aa.data, bb.tolist())
      test3DSlice2(aa, bb)
    #DebugPop()

  def test3DSlice2(AA, BB):
    x,y,z = 0,0,0
    for x in range(AA.shape[0]):
      checkAndPrint(hprt(Checking='X', x=x,y=y,z=z))
      aa = AA[x,:,:]
      bb = BB[x,:,:]
      # DebugPush('print', Array, Array.getitemHelper, 'matmul', unFlattenArray, 'unFlattenArray2', getOffsetOf)
      aal = aa.data
      bbl = bb.tolist()
      # DebugPop()
      checkAndPrint(aal, bbl)

    z = 0
    for x in range(AA.shape[0]):
      for y in range(AA.shape[1]):
        checkAndPrint(hprt(Checking='X and Y', x=x,y=y,z=z))
        aa = AA[:,y,:]
        bb = BB[:,y,:]
        checkAndPrint(aa.data, bb.tolist())
        aa = AA[x,y,:]
        bb = BB[x,y,:]
        checkAndPrint(aa.data, bb.tolist())

    for x in range(AA.shape[0]):
      for y in range(AA.shape[1]):
        for z in range(AA.shape[2]):
          checkAndPrint(hprt(Checking='All3', x=x,y=y,z=z))
          checkAndPrint(hprt(Checking='X', x=x,y=y,z=z))
          aa = AA[x,:,:]
          bb = BB[x,:,:]
          # DebugPush('print', Array, Array.getitemHelper, 'matmul', unFlattenArray, 'unFlattenArray2', getOffsetOf)
          aa = AA[:,y,:]
          bb = BB[:,y,:]
          checkAndPrint(aa.data, bb.tolist())
          aa = AA[:,:,z]
          bb = BB[:,:,z]
          checkAndPrint(aa.data, bb.tolist())
          aa = AA[x,y,:]
          bb = BB[x,y,:]
          checkAndPrint(aa.data, bb.tolist())
          aa = AA[:,y,z]
          bb = BB[:,y,z]
          checkAndPrint(aa.data, bb.tolist())
          aa = AA[x,:,z]
          bb = BB[x,:,z]
          checkAndPrint(aa.data, bb.tolist())
          checkAndPrint(AA[x,y,z].data, BB[x,y,z].item())

  AA = Array(A0)
  BB = torch.tensor(A0)


  test3DSlice(A0)
  checkAndPrint(f'{str(AA)=} {AA.shape=}', "str(AA)='[ [ [ 0 1 2 3 4 ] [ 5 6 7 8 9 ] [ 10 11 12 13 14 ] ] [ [ 15 16 17 18 19 ] [ 20 21 22 23 24 ] [ 25 26 27 28 29 ] ] [ [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] [ 40 41 42 43 44 ] ] ]' AA.shape=[3, 3, 5]")
  B = AA[0]
  checkAndPrint(f'{str(B)=} {B.shape=}', "str(B)='[ [ 0 1 2 3 4 ] [ 5 6 7 8 9 ] [ 10 11 12 13 14 ] ]' B.shape=[3, 5]")
  B = AA[1]
  checkAndPrint(f'{str(B)=} {B.shape=}', "str(B)='[ [ 15 16 17 18 19 ] [ 20 21 22 23 24 ] [ 25 26 27 28 29 ] ]' B.shape=[3, 5]")
  B = AA[2]
  checkAndPrint(f'{str(B)=} {B.shape=}', "str(B)='[ [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] [ 40 41 42 43 44 ] ]' B.shape=[3, 5]")

  B = AA[0:]
  checkAndPrint(f'{str(B)=} {B.shape=}', "str(B)='[ [ [ 0 1 2 3 4 ] [ 5 6 7 8 9 ] [ 10 11 12 13 14 ] ] [ [ 15 16 17 18 19 ] [ 20 21 22 23 24 ] [ 25 26 27 28 29 ] ] [ [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] [ 40 41 42 43 44 ] ] ]' B.shape=[3, 3, 5]")
  B = AA[1:]
  checkAndPrint(f'{str(B)=} {B.shape=}', "str(B)='[ [ [ 15 16 17 18 19 ] [ 20 21 22 23 24 ] [ 25 26 27 28 29 ] ] [ [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] [ 40 41 42 43 44 ] ] ]' B.shape=[2, 3, 5]")
  # DebugPush('print', Array.getitemHelper, Array, getOffsetOf)
  B = AA[2:]
  checkAndPrint(f'{str(B)=} {B.shape=}', "str(B)='[ [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] [ 40 41 42 43 44 ] ]' B.shape=[3, 5]")


  AT = torch.tensor(A0)
  D = AT[0]
  E = AT[1]

  checkAndPrint(f'{str(D)=} {D.shape=}', r"str(D)='tensor([[ 0,  1,  2,  3,  4],\n        [ 5,  6,  7,  8,  9],\n        [10, 11, 12, 13, 14]])' D.shape=torch.Size([3, 5])")
  checkAndPrint(f'{str(E)=} {E.shape=}', r"str(E)='tensor([[15, 16, 17, 18, 19],\n        [20, 21, 22, 23, 24],\n        [25, 26, 27, 28, 29]])' E.shape=torch.Size([3, 5])")
  # DebugPop()
  C = AT[:,0]
  checkAndPrint(f' {C.shape=} {str(C)=}', r" C.shape=torch.Size([3, 5]) str(C)='tensor([[ 0,  1,  2,  3,  4],\n        [15, 16, 17, 18, 19],\n        [30, 31, 32, 33, 34]])'")
  ## DebugPush(Array.__getitem__, Array)

  C = AA[:,0]
  checkAndPrint(f' {C.repr=} {str(C)=}', " C.repr='shape:[3, 5],stride:[15, 1],offset:[0, 0, 0]' str(C)='[ [ 0 1 2 3 4 ] [ 15 16 17 18 19 ] [ 30 31 32 33 34 ] ]'")

  C = AT[:,2]
  checkAndPrint(f' {C.shape=} {str(C)=}', r" C.shape=torch.Size([3, 5]) str(C)='tensor([[10, 11, 12, 13, 14],\n        [25, 26, 27, 28, 29],\n        [40, 41, 42, 43, 44]])'")
  # ## DebugPush(Array.__getitem__, Array)
  # AA = Array(A0)
  C = AA[:,2]
  checkAndPrint(f'{C.repr=}' + f'{str(C)=}', "C.repr='shape:[3, 5],stride:[15, 1],offset:[0, 0, 10]'str(C)='[ [ 10 11 12 13 14 ] [ 25 26 27 28 29 ] [ 40 41 42 43 44 ] ]'")

  C = AT[:,1:]
  checkAndPrint(f' {C.shape=} {str(C)=}', r" C.shape=torch.Size([3, 2, 5]) str(C)='tensor([[[ 5,  6,  7,  8,  9],\n         [10, 11, 12, 13, 14]],\n\n        [[20, 21, 22, 23, 24],\n         [25, 26, 27, 28, 29]],\n\n        [[35, 36, 37, 38, 39],\n         [40, 41, 42, 43, 44]]])'")
  # ## DebugPush(Array.__getitem__, Array)
  C = AA[:,1:]
  checkAndPrint(f'{C.repr=} {str(C)=}', "C.repr='shape:[3, 2, 5],stride:[15, 5, 1],offset:[0, 1, 0, 0]' str(C)='[ [ [ 5 6 7 8 9 ] [ 10 11 12 13 14 ] ] [ [ 20 21 22 23 24 ] [ 25 26 27 28 29 ] ] [ [ 35 36 37 38 39 ] [ 40 41 42 43 44 ] ] ]'")

  # DebugPush('print', Array.getitemHelper, Array)
  C = AA[0,1]
  checkAndPrint(f'{C.repr=} {str(C)=}', "C.repr='shape:[5],stride:[1],offset:[0, 5]' str(C)='[ 5 6 7 8 9 ]'")
  # AT = torch.tensor(A0)
  B =  AT[0,1]
  checkAndPrint(f'{B.shape=} {str(B)=}', "B.shape=torch.Size([5]) str(B)='tensor([5, 6, 7, 8, 9])'")

  # DebugPush('print', Array.getitemHelper, Array)
  C = AA[0,:,1]
  checkAndPrint(f'{C.repr=} {str(C)=}', "C.repr='shape:[3],stride:[5],offset:[0, 1]' str(C)='[ 1 6 11 ]'")
  # AT = torch.tensor(A0)
  B =  AT[0,:,1]
  checkAndPrint(f'{B.shape=} {str(B)=}', r"B.shape=torch.Size([3]) str(B)='tensor([ 1,  6, 11])'")


  #DebugPush('print', Array.getitemHelper, Array)
  C = AA[1,:,0]
  checkAndPrint(f'{C.repr=} {str(C)=}', "C.repr='shape:[3],stride:[5],offset:[0, 15]' str(C)='[ 15 20 25 ]'")
  # AT = torch.tensor(A0)
  B =  AT[1,:,0]
  checkAndPrint(f'{B.shape=} {str(B)=}', "B.shape=torch.Size([3]) str(B)='tensor([15, 20, 25])'")

  # DebugPush('print', Array.getitemHelper, Array)
  C = AA[1,:,1:3]
  # print(C.shape.__class__)
  checkAndPrint(f'{C.shape=} {str(C)=}', "C.shape=[3, 2] str(C)='[ [ 16 17 ] [ 21 22 ] [ 26 27 ] ]'")
  # AT = torch.tensor(A0)
  B =  AT[1,:,1:3]
  checkAndPrint(f'{B.shape=} {str(B)=}', r"B.shape=torch.Size([3, 2]) str(B)='tensor([[16, 17],\n        [21, 22],\n        [26, 27]])'")

  # now check for assignment,
  # DebugPush('print', Array.getitemHelper, Array.__setitem__, Array)
  A = Array([0,1,2,3,4])
  B = torch.tensor([0,1,2,3,4])
  A[0] = -1
  B[0] = -1
  checkAndPrint(f'{A.repr} {str(A)}', "shape:[5],stride:[1],offset:[0, 0] [ -1 1 2 3 4 ]")
  checkAndPrint(f'{B.shape} {str(B)}', "torch.Size([5]) tensor([-1,  1,  2,  3,  4])")
  A[0] = 0
  B[0] = 0
  checkAndPrint(f'{A.repr} {str(A)}', "shape:[5],stride:[1],offset:[0, 0] [ 0 1 2 3 4 ]")
  checkAndPrint(f'{B.shape} {str(B)}', "torch.Size([5]) tensor([0, 1, 2, 3, 4])")
  #DebugPush('print', Array.getitemHelper, Array.__setitem__, Array)

  A[0:2] = [-1,-2]
  B[0:2] = torch.tensor([-1,-2])
  checkAndPrint(f'{A.repr} {str(A)}', "shape:[5],stride:[1],offset:[0, 0] [ -1 -2 2 3 4 ]")
  checkAndPrint(f'{B.shape} {str(B)}', "torch.Size([5]) tensor([-1, -2,  2,  3,  4])")
  A[:] = [-5,-4,-3,-2,-1]
  B[:] = torch.tensor([-5,-4,-3,-2,-1])
  checkAndPrint(f'{A.repr} {str(A)}', "shape:[5],stride:[1],offset:[0, 0] [ -5 -4 -3 -2 -1 ]")
  checkAndPrint(f'{B.shape} {str(B)}', "torch.Size([5]) tensor([-5, -4, -3, -2, -1])")
  
  # DebugPush('print', Array.getitemHelper, Array.__setitem__, Array)
  A = Array(9)
  B = torch.tensor(9)
  checkAndPrint(f'{str(A)} {str(B)}', "9 tensor(9)")

  A = Array([0,1,2,3,4])
  A[:] = Array([-5,-4,-3,-2,-1])
  checkAndPrint(f'{A.repr} {str(A)}', "shape:[5],stride:[1],offset:[0, 0] [ -5 -4 -3 -2 -1 ]")

  # DebugPush('print', Array.getitemHelper, unFlattenArray)
  # print(f'{A0=}')
  A = Array([[0,1,2],[3,4,5], [6,7,8]])
  B = unFlattenArray(A.shape, A.stride, A.offset, A.item)
  checkAndPrint(f'{str(A)} {B}', "[ [ 0 1 2 ] [ 3 4 5 ] [ 6 7 8 ] ] [[0, 1, 2], [3, 4, 5], [6, 7, 8]]")

  # shape is [3,4,5]
  A1 = [
    [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14],[15,16,17,18,19]],
    [[20,21,22,23,24],[25,26,27,28,29],[30,31,32,33,34],[35,36,37,38,39]],
    [[40,41,42,43,44],[45,46,47,48,49],[50,51,52,53,54],[55,56,57,58,59]],
    ]
  # DebugPush('print', Array.permute)
  test3DSlice(A1)
  # DebugPop()
  A = Array(A0)
  B = unFlattenArray(A.shape, A.stride, A.offset, A.item)
  checkAndPrint(B, A0)
  # DebugPush('print', Array.getitemHelper, unFlattenArray)
  A = Array(A1)
  B = unFlattenArray(A.shape, A.stride, A.offset, A.item)
  checkAndPrint(B, A1)

  B = A.view(-1)
  checkAndPrint(f'{B}', '[ 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 ]')
  B[0] = -16
  checkAndPrint(f'{B}', '[ -16 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 ]')
  # DebugPush('print', Array.getitemHelper, Array, unFlattenArray, Array.view)
  C = A[0,0,0]
  checkAndPrint(f'{C.repr}', 'shape:[],stride:[],offset:[0]')
  checkAndPrint(C, '-16')
  D = A[0,0]
  checkAndPrint(D, '[ -16 1 2 3 4 ]')
  A = Array(A1)
  D = A[0,0]
  checkAndPrint(D, '[ 0 1 2 3 4 ]')
  B = A.view(10,-1)
  checkAndPrint(f'{B.repr} {str(B)=}', r"shape:[10, 6],stride:[6, 1],offset:[0, 0, 0] str(B)='[ [ 0 1 2 3 4 5 ] [ 6 7 8 9 10 11 ] [ 12 13 14 15 16 17 ] [ 18 19 20 21 22 23 ] [ 24 25 26 27 28 29 ] [ 30 31 32 33 34 35 ] [ 36 37 38 39 40 41 ] [ 42 43 44 45 46 47 ] [ 48 49 50 51 52 53 ] [ 54 55 56 57 58 59 ] ]'")
  T = torch.tensor(A1)
  C = T.view(10,-1)
  checkAndPrint(f'{C.shape=}', "C.shape=torch.Size([10, 6])")
  # DebugPush('print', Array.getitemHelper, Array, unFlattenArray, Array.view, Array.permute)
  
  C1 = A[1]
  checkAndPrint(f'{C1.repr=} {str(C1)=}', "C1.repr='shape:[4, 5],stride:[5, 1],offset:[0, 0, 20]' str(C1)='[ [ 20 21 22 23 24 ] [ 25 26 27 28 29 ] [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] ]'")
  D1 = C1.permute(1,0)
  checkAndPrint(f'{D1.repr=} {str(D1)=}', "D1.repr='shape:[5, 4],stride:[1, 5],offset:[0, 0, 20]' str(D1)='[ [ 20 25 30 35 ] [ 21 26 31 36 ] [ 22 27 32 37 ] [ 23 28 33 38 ] [ 24 29 34 39 ] ]'")

  C2 = T[1]
  checkAndPrint(f'{C2.shape=}{str(C2)=}', r"C2.shape=torch.Size([4, 5])str(C2)='tensor([[20, 21, 22, 23, 24],\n        [25, 26, 27, 28, 29],\n        [30, 31, 32, 33, 34],\n        [35, 36, 37, 38, 39]])'")
  D2 = C2.permute(1,0)
  checkAndPrint(f'{D2.shape=} {str(D2)=}', r"D2.shape=torch.Size([5, 4]) str(D2)='tensor([[20, 25, 30, 35],\n        [21, 26, 31, 36],\n        [22, 27, 32, 37],\n        [23, 28, 33, 38],\n        [24, 29, 34, 39]])'")

  # DebugPush('print', Array.getitemHelper, Array, unFlattenArray, Array.view, Array.permute, Array.__setitem__)
  C1[1,0] = -17
  checkAndPrint(f'{C1.repr=} {str(C1)=}', "C1.repr='shape:[4, 5],stride:[5, 1],offset:[0, 0, 20]' str(C1)='[ [ 20 21 22 23 24 ] [ -17 26 27 28 29 ] [ 30 31 32 33 34 ] [ 35 36 37 38 39 ] ]'")

  # DebugPush('print', Array.getitemHelper, Array, unFlattenArray, Array.view, Array.permute, Array.__setitem__)
  ANS = [ lhv.data * rhv.data for lhv, rhv in zip([ k for k in Array([0,1,2,3])] , [ v for v in Array([4,5,6])])  ]
  checkAndPrint(ANS, [0, 5, 12])
  AAA = Array([0,1,2,3])
  BBB = Array([4,5,6])
  #DebugPush('print', Array.getitemHelper, Array, unFlattenArray, Array.view, Array.permute, Array.__setitem__)
  ANS = [ lhv * rhv for lhv, rhv in zip([ k for k in AAA.data ] , [ v for v in BBB.data])  ]
  checkAndPrint(ANS, [0, 5, 12])

  AAA = Array(4)

  ANS = Array([ lhv.data * rhv.data for lhv, rhv in zip([ k for k in AAA ] , [ v for v in BBB]) ], AAA.shape)
  checkAndPrint(f'{ANS},{ANS.shape}', '16,[]')


def TestArray02():
  # shape is [3,4,5]
  A1 = [
    [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14],[15,16,17,18,19]],
    [[20,21,22,23,24],[25,26,27,28,29],[30,31,32,33,34],[35,36,37,38,39]],
    [[40,41,42,43,44],[45,46,47,48,49],[50,51,52,53,54],[55,56,57,58,59]],
    ]
  AA = Array(A1)
  



def TestTensor():
  # DebugPush('print')
  checkAndPrint(Tensor.isBroadcastable([5,1,4,1], [3,1,1]), [5,3,4,1])
  checkAndPrint(Tensor.isBroadcastable([1], [3,1,7]), [3,1,7])
  checkAndPrint(Tensor.isBroadcastable([5,2,4,1], [3,1,1]), None)
  checkAndPrint(Tensor.isBroadcastable([0], [2,2]), None)
  checkAndPrint(Tensor.isBroadcastable([5,3,4,1], [3,1,1]), [5,3,4,1])
  checkAndPrint(Tensor.isBroadcastable([5,2,4,1], [3,1,1]), None)

  def testAdd(ValType):

    x = ValType(-4.0, requires_grad=True); assignLabel(x, 'x')
    z = 2*x+2 + x; assignLabel(z, 'z')
    q = z.relu() + z  * x; assignLabel(q, 'q')
    h = (z*z).relu(); assignLabel(h, 'h')
    y = h + q + q * x; assignLabel(y, 'y')

    return (y,h, q, z, x) # (y, h, q, z, x)

  # DebugPop()
  # DebugPush(Tensor, Tensor.binaryOpHelper, Tensor.backward, Array, Array.getitemHelper, 'matmul', Array.view)
    

  #DebugPush('print', Array.getitemHelper, Array, unFlattenArray, Array.view, Array.permute)
  A1 = [
    [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14],[15,16,17,18,19]],
    [[20,21,22,23,24],[25,26,27,28,29],[30,31,32,33,34],[35,36,37,38,39]],
    [[40,41,42,43,44],[45,46,47,48,49],[50,51,52,53,54],[55,56,57,58,59]],
    ]
  A11 = Array(A1)
  #DebugPush('print')
  checkAndPrint(A11.data, A1)

  # DebugPush('print', Tensor.backward, Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.relu)
  checkAndPrint('**************************** START FIRST BACKWARD ********')
  ANS0 = testAdd(Value)
  ANS0[0].backward()

  ANS1 = testAdd(Tensor)
  y1, h1, q1, z1, x1 = ANS1
  # assert y1.expr.item == operator.add
  # assert y1.expr.kid_names == [0,1,2]
  # assert y1.expr.kids[0].item == y1
  # assert isinstance(y1.expr.kids[0], Tree)
  # assert isinstance(y1.expr.kids[1], Tree)
  # assert isinstance(y1.expr.kids[2], Tree)
  ANS1[0].backward()

  # assert x1.grad.item() == 2.0
  # print('TOP:', y1.expr.item, y1.expr.item.__class__)
  # for kn in y1.expr.kid_names:
  #   print(f'  {kn=}:')
  # print(hprt(y1Expr=y1.expr.to_string()))
  ANS2 = testAdd(torch.tensor)
  y2, h2, q2, z2, x2 = ANS2
  for i, k in enumerate(ANS2):
    k.retain_grad()
  y2.backward()
  assert len(ANS0) == len(ANS1) and len(ANS1) == len(ANS2), f'mismatching answer {len(ANS0)=} {len(ANS1)=} {len(ANS2)=}'
  for i in range(len(ANS0)):
    assert ANS0[i].data == ANS1[i].item() and ANS1[i].item() == ANS2[i].item(), \
    f'mismatching answer at {i=} {ANS0[i]=} {ANS1[i]=} {ANS2[i]=}'
    checkAndPrint(f'{i=} {ANS0[i]=} {ANS0[i].grad=} {ANS1[i]=} {ANS2[i]=} {ANS2[i].grad=}')
    # checkAndPrint(ANS1[i].item(), ANS2[i].item())
    # checkAndPrint(ANS1[i].grad.item(), ANS2[i].grad.item())
  checkAndPrint(y2.item(), y1.item())
  assert y2.item() == -20
  assert y1.item() == y2.item()
  # assert y1.grad.item() == y2.grad.item()

  checkAndPrint(y2.grad.item(), y1.grad.item())
  # print(f'{h2.grad=} {h1.grad=}')
  # print(f'{y2.grad=} {y1.grad=}')
  checkAndPrint(z2.grad.item(), z1.grad.item())
  checkAndPrint(x2.grad.item(), x1.grad.item())
  checkAndPrint(x1.grad.item(), x2.grad.item())
  # print(f'{y1.grad.item() - y2.grad.item()=:6f}')
  checkAndPrint('**************************** END FIRST BACKWARD ********')
  # DebugPop()

  x = Tensor(-18)
  # print(x, x.data)
  x = [ v.data for v in A11]
  # DebugPush('print')
  # 1760
  checkAndPrint2('ADD()', ADD(*x), ADD(*range(60)))
  T1 = Tensor(A1)
  checkAndPrint2('3d shape ', T1.shape, [3,4,5])
  # DebugPush('print', Array.Iter, Array)
  
  T0 = torch.tensor(A1)

  Dims = [2,4,3]
  x0 = Tensor.zeros(*Dims)
  x1 = torch.zeros(*Dims)
  checkAndPrint2('Tensor.shape', x0.shape, Dims)
  checkAndPrint2('torch.Tensor.shape', list(x1.shape), Dims)
  checkAndPrint2('simple access', type(x0[0,0,0].item()), float)
  checkAndPrint2('simple access', type(x1[0,0,0].item()), float)
  checkAndPrint2('sum', x0.sum().item(), x1.sum().item())

  x0 = Tensor.ones(*Dims)
  x1 = torch.ones(*Dims)
  checkAndPrint(x0.sum().item(), x1.sum().item())

  x0 = Tensor.arange(6)
  x1 = torch.arange(6)
  checkAndPrint(hprt(x0=x0.shape, x1=x1.shape))
  checkAndPrint(hprt(x0=x0, x1=x1))
  checkAndPrint(x0.sum().item(), x1.sum().item())

  # DebugPush('print')
  x0 = Tensor.arange(0.5, 6, dtype=float) *.5
  x1 = torch.arange(0.5, 6, dtype=float) *.5
  checkAndPrint(hprt(x0=x0.shape, x1=x1.shape))
  checkAndPrint(hprt(x0=x0, x1=x1))
  #print (x0.shape, x0[2].shape)
  # DebugPop()

  #DebugPush('print', Array, Array.getitemHelper, 'matmul')
  for r in range(x0.shape[0]):
    #print(f'{r=} {x0[r].item()=}', f'{r=} {x1[r].item()=}')
    checkAndPrint(f'{r=} {x0[r].item()}', f'{r=} {x1[r].item()}')
  checkAndPrint(x0.sum().item(), x1.sum().item())
  #DebugPop()
  # (2,3) =  (2,4) @ (4,3)
  # (a,b) =  (a,c) @ (c,b)


  checkAndPrint('Now Starting MatMul')
  def matMul000(ValType):
    TX = ValType([[1,2,3],[4,5,6]], dtype=float, requires_grad = True); assignLabel(TX, 'TX')
    V = TX + 1; assignLabel(V, 'V')
    Q = V.sum(); assignLabel(Q, 'Q')
    Q.backward()
    checkAndPrint(hprt(TXG=TX.grad))
    return Q, V, TX
  
  ANS0 = matMul000(torch.tensor)
  ANS1 = matMul000(Tensor)
  checkAndPrint(f'{ANS0[0].item():.4f}', f'{ANS1[0].item():.4f}')
  for i in range(1,len(ANS0)):
    checkAndPrint(list(map(int, flattenArray(ANS0[i].tolist()))), list(map(int, flattenArray(ANS1[i].data.data))))
  def matMul00(ValType, ARange):

    TX = ValType([[1,2],[3,4]], dtype=float, requires_grad = True); assignLabel(TX, 'TX')
    V = ValType([[1.5,0],[0,1.5]], dtype=float); assignLabel(V, 'V')
    Z = TX @ V;  assignLabel(Z, 'Z')
    Z.retain_grad()
    Q = Z.sum(); assignLabel(Q, 'Q')
    Q.backward()
    checkAndPrint(hprt(Q=Q, Z=Z, ZG=Z.grad, TXG=TX.grad, VG=V.grad))
    return Q, TX

  Q0 = matMul00(torch.tensor, torch.arange)
  # DebugPop()
  checkAndPrint(matMul00)  
  Q1 = matMul00(Tensor, Tensor.arange)
  assert Q0[0].item() == Q1[0].item(), f'{Q0[0]=} != {Q1[0]=}'
  assert list(Q0[1].grad.shape) == Q1[1].grad.shape, f'mismatching shapes {Q0[1].grad.shape} {Q1[1].grad.shape}'
  for r in range(Q0[1].shape[0]):
    for c in range(Q0[1].shape[1]):
      LL =  Q0[1].grad[r,c].item(), Q1[1].grad[r,c].item()
      checkAndPrint(LL[0], LL[1])

  checkAndPrint(Q0[1].grad.sum().item(), Q1[1].grad.sum().item())
  
  def matMul01(ValType, ARange):

    TX = ValType([[1,2,3],[4,5,6]], dtype=float, requires_grad = True); assignLabel(TX, 'TX')
    V = ARange(0.5, 3, dtype=float); assignLabel(V, 'V')
    V.requires_grad = True
    Z = TX @ V;  assignLabel(Z, 'Z')
    checkAndPrint(f'Z{list(Z.shape)} = TX{list(TX.shape)} @ V{list(V.shape)} {(ValType==Tensor)=}')
    Z.retain_grad()
    Q = Z.sum(); assignLabel(Q, 'Q')
    Q.backward()
    checkAndPrint(hprt(Z=vprt(list(Z.shape), Z), EQUALS='',
                       TX=vprt(list(TX.shape), TX), AT='@', V=vprt(list(V.shape), V)))
    checkAndPrint(vprt('AFTER BACKWARD PASS',
                       hprt(ZG=vprt(ZGShape=list(Z.grad.shape),ZGrad=Z.grad, ZS=list(Z.shape),Z=Z),
                            TXG=vprt(TXGS=list(TX.grad.shape), TXG=TX.grad, TXS=list(TX.shape), TX=TX),
                            VG=vprt(VGS=list(Z.grad.shape),VG=V.grad, VS=list(V.shape), V=V))))

    return Q, TX

  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward)
  Q0 = matMul01(torch.tensor, torch.arange)
  Q1 = matMul01(Tensor, Tensor.arange)

  assert Q0[0].item() == Q1[0].item(), f'{Q0[0]=} != {Q1[0]=}'
  assert list(Q0[1].grad.shape) == Q1[1].grad.shape, f'mismatching shapes {Q0[1].grad.shape} {Q1[1].grad.shape}'
  checkAndPrint(matMul01)
  for r in range(Q0[1].shape[0]):
    for c in range(Q0[1].shape[1]):
      checkAndPrint(Q0[1].grad[r,c].item(), Q1[1].grad[r,c].item())
  checkAndPrint(Q0[1].grad.sum().item(), Q1[1].grad.sum().item())

  def matMul02(ValType, ARange):
    k1 = ARange(6).view(3,2)
    k2 = ARange(6).view(2,3)
    k3 = k1 @ k2
    checkAndPrint(hprt(IsTensor=ValType==Tensor, k1=k1,k2=k2,k3=k3, k3s=k3.shape))
    return k3
  k3A = matMul02(torch.tensor, torch.arange)
  k3B = matMul02(Tensor, Tensor.arange)
  checkAndPrint(list(k3A.shape), k3B.shape)
  checkAndPrint(list(k3A.tolist()), k3B.data.data)


  def matMul03(ValType, r,c):
    ''' do [r] @ [r,c] -> [1,r][r,c]
    '''
    A = ValType(list(range(r)))
    B = ValType(list(range(r*c))).view(r,c)
    C1 = A @ B
    checkAndPrint(f'{(ValType==Tensor)=} {r=},{c=} {list(C1.shape)}={list(A.shape)}@{list(B.shape)}')
    C2 = A @ A
    checkAndPrint(f'{(ValType==Tensor)=} {r=},{c=} {list(C2.shape)}={list(A.shape)}@{list(A.shape)}')
    if r == c:
      C3 = B * B
      checkAndPrint(f'{(ValType==Tensor)=} {r=},{c=} {list(C3.shape)}={list(B.shape)}@{list(B.shape)}')
    else:
      C3 = B.T @ B
      checkAndPrint(f'{(ValType==Tensor)=} {r=},{c=} {list(C3.shape)}={list(B.T.shape)}@{list(B.shape)}')
    return C1,C2,C3

  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward)  
  X0 = matMul03(torch.tensor, 3,4)
  X1 = matMul03(Tensor, 3,4)
  for i in range(len(X0)):
    checkAndPrint(list(X0[i].shape), list(X1[i].shape))
    checkAndPrint(X0[i].tolist(), X1[i].data.data)

  def testMatMul(ValType, Zeros, ARange, Ones, Dims):
    checkAndPrint(f'doing testMatMul with {Dims=} and {(ValType==Tensor)=}')
    C = Zeros(Dims[0],Dims[1], dtype=float); assignLabel(C, 'C')
    D = ARange(float(Dims[0]*Dims[1]), dtype=float).view(Dims[0],Dims[1]);  assignLabel(D, 'D')
    E = Ones(Dims[2],Dims[1], dtype=float)*2; assignLabel(E, 'E')
    # DebugPush('print',  'matmul2', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward, Tensor.relu)
    F = ARange(0.5,Dims[1], dtype=float)* .1;  assignLabel(F, 'F')
    # DebugPop()
    checkAndPrint(hprt(F=F, E=E, D=D, C=C))
    params = [D, E, F]
    for p in params:
      p.requires_grad = True
      p.retain_grad()
    G = (D @ F).relu(); assignLabel(G, 'G')
    S = G.sum(); assignLabel(S, 'S')
    checkAndPrint('DONE')
    checkAndPrint(hprt(S=S, G=G))
    return S, G, F, E, D, C

  # print('YES')
  # DebugPush('print') # , Array, Array.getitemHelper, 'matmul', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper)
  x0a = torch.arange(0.5,Dims[1], dtype=float)
  x1a = Tensor.arange(0.5,Dims[1], dtype=float)
  # print(f'{type(x0a)=}, {type(x1a)=}')
  checkAndPrint(hprt(x0a=x0a, x1a=x1a))
  #DebugPop()
  # DebugPush('print', Array, getOffsetOf, Array.getitemHelper, 'matmul', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper)
  #print('YES2')
  x0 = x0a * 0.1
  x1 = x1a * 0.1
  checkAndPrint(hprt(x0=x0, x1=x1))
  L0 = x0.tolist()
  L1 = x1.data.data
  for v0, v1 in zip(L0, L1):
    checkAndPrint(f'{v0:.4f}', f'{v1:.4f}')
  # print('YES3')
  #DebugPop()
  
  # DebugPush('print')
  checkAndPrint('***** NOW STARTING testMatMul')
  ANS0 = testMatMul(torch.tensor, torch.zeros,
                    torch.arange, torch.ones, Dims)
  S0, G0, F0, E0, D0, C0 = ANS0
  S0.retain_grad()
  S0.backward()

  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.relu, 'matmul', Tensor.backward)
  ANS1 = testMatMul(Tensor, Tensor.zeros,
                    Tensor.arange, Tensor.ones, Dims)
  S1, G1,F1,E1,D1,C1 = ANS1
  S1.retain_grad()
  S1.backward()
  checkAndPrint(vprt(Job=testMatMul,
                     pytorch=hprt(S=S0, DG=D0.grad, EG=E0.grad, FG=F0.grad),
                     Tensor=hprt(S=S1, DG=D1.grad, EG=E1.grad, FG=F1.grad)))
  for r in range(D1.shape[0]):
    for c in range(D1.shape[1]):
      checkAndPrint(f'{r=},{c=} {D0.grad[r,c].item():.4f}',
                    f'{r=},{c=} {D1.grad[r,c].item():.4f}')
  checkAndPrint(list(F0.grad.shape), F1.grad.shape)
  # ANS1 = G1.sum()
  # checkAndPrint(ANS1)
  # ANS1.retain_grad()
  # ANS1.backward()
  # checkAndPrint(hprt(ANS=ANS1, DGrad=D1.grad, EGrad=E1.grad, FGrad=F1.grad))



def testSanityChecks():
# DebugPush('print')
  def test_sanity_check(ValType, **kwargs):
    x = ValType(-4.0, **kwargs)
    z = 2 * x + 2 + x
    q = z.relu() + z * x
    h = (z * z).relu()
    y = h + q + q * x
    y.backward()
    return y,x
  
  ANS = test_sanity_check(Value), \
    test_sanity_check(Tensor, requires_grad=True, dtype=float), \
    test_sanity_check(torch.tensor, requires_grad=True, dtype=float)
  
  checkAndPrint(ANS[0][0].data, ANS[1][0].item())
  checkAndPrint(ANS[1][0].data, ANS[2][0].item())
  
  checkAndPrint(ANS[0][1].grad, ANS[1][1].grad.item())
  checkAndPrint(ANS[1][1].grad.item(), ANS[2][1].grad.item())
  
  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.backward, Tensor.unaryOpHelper)
  def test_more_ops(ValType, **kwargs):
    a = ValType(-4.0, **kwargs)
    b = ValType(2.0, **kwargs)
    c = a + b
    d = a * b + b**3
    c += c + 1
    c += 1 + c + (-a)
    d += d * 2 + (b + a).relu()
    d += 3 * d + (b - a).relu()
    e = c - d
    f = e**2
    g = f / 2.0
    g += 10.0 / f
    g.backward()
    return g, b, a
  
  ANS2 = test_more_ops(Value), \
    test_more_ops(Tensor, dtype=float, requires_grad=True), \
    test_more_ops(torch.tensor, dtype=float, requires_grad=True)
  
  checkAndPrint('**** test_more_ops')
  checkAndPrint(ANS2[0][0].data, ANS2[1][0].item())
  checkAndPrint(ANS2[1][0].data, ANS2[2][0].item())
  
  checkAndPrint(f'{ANS2[0][1].grad:.4f}', f'{ANS2[1][1].grad.item():.4f}')
  checkAndPrint(f'{ANS2[1][1].grad.item():.4f}', f'{ANS2[2][1].grad.item():.4f}')
  
  checkAndPrint(f'{ANS2[0][2].grad:.4f}', f'{ANS2[1][2].grad.item():.4f}')
  checkAndPrint(f'{ANS2[1][2].grad.item():.4f}', f'{ANS2[2][2].grad.item():.4f}')
  # DebugPop()
  # K0 = Tensor([ i//10.0 for i in range(100)])
  # print(K0, K0.tanh())
  
  def testMoreMatMul(ValType, Len, shape1, shape2):
    K = ValType(list(range(Len)), dtype=float).view(*shape1)
    K.requires_grad =  True
    J = ValType([1]*Len, dtype=float).view(*shape2)
    J.requires_grad = True
    checkAndPrint(Shapes(K=K, J=J))
  
    Y = K@J
    Z = Y.sum()
    Z.retain_grad()
    Z.backward()
    return Z, Y, J, K
  
  # TT0 = Array([0,1,2,3,4])
  # TT1 = TT0.view(-1)
  # print(Shapes(TT0=TT0, TT1=TT1))
  
  # DebugPush('print')
  SS = [([-1],[-1]), ([1, -1], [-1, 1]), ([-1, 1], [1, -1]) ];
  for sh1, sh2 in SS:
    checkAndPrint(f'checking MoreMatMul {sh1=} {sh2=}')
    P1 = testMoreMatMul(torch.tensor, 5, sh1, sh2)
    P2 = testMoreMatMul(Tensor, 5, sh1, sh2)
    for i in range(len(P1)):
      checkAndPrint2(f'P1[{i}].shape vs P2[{i}].shape', list(P1[i].shape), list(P2[i].shape))
      T1 = P1[i]
      T2 = P2[i]
      A1 = list(flattenArray(T1.tolist(), float))
      A2 = list(flattenArray(T2.tolist(), float))
      # if isinstance(A1, list):
      #   assert type(A1[0]) == type(A2[0]), f'{type(A1[0])} != {type(A2[0])}'
      # else:
      #   assert type(A1) == type(A2), f'{type(A1)} != {type(A2)}'

      checkAndPrint(A1, A2)
  
  def ShapeCheck(ValType):
    K = ValType(16)
    J = ValType([16])
    Y = K+J
    return Y,J,K
  
  #DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward)
  
  A1 = ShapeCheck(torch.tensor)
  A2 = ShapeCheck(Tensor)
  for i in range(len(A1)):
    T1 = A1[i]
    T2 = A2[i]
    checkAndPrint2(f'shapes of {i}', list(T1.shape), list(T2.shape))
    checkAndPrint2(f'tolist of {i}', T1.tolist(), T2.tolist())
    checkAndPrint2(f'item of {i}', T1.item(), T2.item())
  
  def ShapeCheck2(ValType):
    K = ValType(16)
    J = ValType([16])
    K += J
    return K
  
  A1 = ShapeCheck(torch.tensor)
  A2 = ShapeCheck(Tensor)
  assert len(A1) == len(A2)
  for i in range(len(A1)):
    checkAndPrint2(f'shapes of {i}', list(T1.shape), list(T2.shape))
    checkAndPrint2(f'tolist of {i}', T1.tolist(), T2.tolist())
    checkAndPrint2(f'item of {i}', T1.item(), T2.item())
  
  def ShapeCheck3(ValType, Len, Shape):
    K = ValType(list(range(Len)), dtype=float).view(*Shape)
    K.requires_grad = True
    L = ValType(list(range(Len)), dtype=float).view(*Shape)
    K += L
    K += ValType(1)
    return K, L
  
  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward)
  K, L = ShapeCheck3(Tensor, 6, [2,3])
  checkAndPrint2(f'inPlaceAdd', K)

  #DebugPop()
import random
import numpy as np

class Layer:
  def __init__(s, id, nin, nout, ValType=torch.tensor, dtype=float):
    s.w = ValType([ random.uniform(-1,1) for k in range(nin*nout)], dtype=dtype).view(nout, nin); assignLabel(s.w, f'L{id}.w', requires_grad=True)
    s.b = ValType([random.uniform(-1,1) for k in range(nout)], dtype=dtype); assignLabel(s.b, requires_grad=True, label=f'L{id}.b')

  def __call__(s, x):
    outs = (s.w @ x + s.b).tanh() 
    return outs
  def parameters(s):
    return [s.w, s.b] 
    
class MLP:
  def __init__(s, nin, nouts, ValType=torch.tensor, dtype=float):
    """ nouts in a list of sizes for the individual layers """
    sz = [nin] + nouts
    s.layers = [Layer(i, sz[i], sz[i+1], dtype=dtype, ValType=ValType) for i in range(len(nouts))]
  def __call__(s,x):
    s.trace = []
    for layer in s.layers:
      x1 = layer(x)
      s.trace.append({ 'src': x, 'w' : layer.w, 'b' : layer.b, 'dst': x1})
      x = x1
    return x
  def parameters(s):
    return [ p for layer in s.layers for p in layer.parameters() ]

def buildMLP(nin, nouts, dtype=float, ValType=torch.tensor):
  n = MLP(nin, nouts, dtype=dtype, ValType=ValType)
  for i,k in enumerate(n.layers):
    checkAndPrint(f'Layer {ValType==torch.tensor=} {i} params: {k.w.shape=}, {k.b.shape=}')
  checkAndPrint(f'number of parameters = {sum(map(lambda x: MUL(*x.shape, 1,1), [ x for x in n.parameters() ]))}')
  return n

def doPass(nn, xs, ys, npass=20, ValType=torch.tensor, Zeros=torch.zeros, dtype=float, LR=0.05):
  assert len(xs) == len(ys), f'{len(xs)=} elements but {len(ys)=}'
  assert isinstance(nn, MLP), f'{type(MLP)} is not a {MLP}'
  # DebugPush('print')
  checkAndPrint(f'using {ValType==torch.tensor=} to run {npass=} on {xs=} {ys=} {LR=}')
  YS = [ ValType(k,dtype=dtype) for k in ys ]
  #ebugPush(Tensor, Tensor.backward, Tensor.binaryOpHelper, Tensor.unaryOpHelper)
  pDL = []
  pCopy = []
  for k in range(npass):
    # forward pass
    # produce len(xs) number of tensors
    ypred = [ nn(ValType(x,dtype=dtype)) for x in xs ]
    loss = sum((yout-ygt)**2 for ygt, yout in zip(YS, ypred))
    checkAndPrint(f'pass {k} {loss.item()}')
    # backward pass
    for p in nn.parameters():
      p.grad = Zeros(*p.shape, dtype=float)
    loss.backward()
    # update
    # note that in a real NN, the following manual adjustment would be handled automatically by
    # pytorch's gradient descent modules.
    for i, p in enumerate(nn.parameters()):
      pD = -LR * p.grad
      pDL.append(pD)
      # checkAndPrint(Shapes(PASS=f'BEFORE {i}', p=p, pD=pD, LR=-LR, PGrad=p.grad))
      assert pD.shape == p.shape

      p.requires_grad = False
      ## note that without a proper implementation of __iadd__()
      ## TinyTorch.Tensor would not be able to handle the += below.
      p += pD
      # checkAndPrint(Shapes(PASS=f'AFTER {i}', p=p, pD=pD, LR=-LR, PGrad=p.grad))
      pCopy.append(ValType(p.tolist(), dtype=p.dtype))
      p.requires_grad = True
  return loss, ypred, pCopy, pDL

def testSimple():
  def testDraw(ValType):
    a = ValType(2.0, label='a', requires_grad=True)
    b = ValType(-3.00, label='b', requires_grad=True)
    c = ValType(10.0, label='c', requires_grad=True)
    e = a*b; 
    d = e + c                                                  ; assignLabel(d, 'd', requires_grad=True) 
    e = a*b                                                    ; assignLabel(e, 'e', requires_grad=True)                                    
    f = ValType(-2.0, label = 'f', requires_grad=True)
    L = d*f                                                    ; assignLabel(L, 'L', requires_grad=True)
    return L,f,e,d,c,b,a
  V1 = testDraw(Value)
  V2 = testDraw(Tensor)
  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward)
  V1[0].backward()
  V2[0].backward()
  for a, b in zip(V1, V2):
    checkAndPrint2(f'simple {a.label}', a.data, b.item())
    checkAndPrint2(f'simple {a.label}.grad', a.grad, b.grad.item())
  # DebugPop()


def backwardTest():

  xs = [
    [2.0, 3.0, -1.0],
    [3.0, -1.0, 0.5],
    [0.5, 1.0, 1.0],
    [1.0, 1.0, -1.0],
  ]
  ys = [ 1.0, -1.0, -1.0, 1.0] # desired targets
  Tensor.multiline = True
  np.random.seed(1337)
  random.seed(1337)
  n1 = buildMLP(3, [4, 4, 1])

  np.random.seed(1337)
  random.seed(1337)
  n2 = buildMLP(3, [4, 4, 1], ValType=Tensor)

  a1 = n1(torch.tensor(xs[0], dtype=float))
  a2 = n2(Tensor(xs[0],dtype=float))
  assert abs(a1.item() - a2.item()) < 0.000001


  # do it again
  np.random.seed(1337)
  random.seed(1337)
  n1 = buildMLP(3, [4, 4, 1])

  np.random.seed(1337)
  random.seed(1337)
  n2 = buildMLP(3, [4, 4, 1], ValType=Tensor)

  # DebugPush('print')
  for i in range(len(n1.layers)):
    L1 = n1.layers[i]
    L2 = n2.layers[i]
    checkAndPrint(list(L1.w.shape), L2.w.shape)
    checkAndPrint(list(L1.b.shape), L2.b.shape)
    checkAndPrint2(f'MLP.layer[{i}].w', L1.w.tolist(), L2.w.tolist())
    checkAndPrint2(f'MLP.layer[{i}].b', L1.b.tolist(), L2.b.tolist())

    for i in range(len(n1.parameters())):
      p1 = n1.parameters()[i]
      p2 = n2.parameters()[i]
      checkAndPrint2(f'MLP.parameter[{i}].shape', list(p1.shape), p2.shape)
      checkAndPrint2(f'MLP.parameter[{i}].tolist()', p1.tolist(), p2.tolist())
  DebugPush('print')
  # DebugPush('print', Tensor, Tensor.binaryOpHelper, Tensor.unaryOpHelper, Tensor.backward)
  l1,y1,z1,m1 = doPass(n1, xs, ys, npass=20)
  l2,y2,z2,m2 = doPass(n2, xs, ys, npass=20, ValType=Tensor, Zeros = Tensor.zeros)
  DebugPop()

  checkAndPrint2('checking losses are same', f'{l1.item():.6f}', f'{l2.item():.6f}')
  # DebugPush('print')
  assert len(n1.trace) == len(n2.trace)
  for i, (r1, r2) in enumerate(zip(n1.trace, n2.trace)):
    if isinstance(r1, dict):
      for j in r1:
        checkAndPrint2(f'shapes of trace [{i} {j}]', list(r1[j].shape), list(r2[j].shape))
        A1, A2 = [ f'{i:.6f}' for i in flattenArray(r1[j].tolist()) ], \
          [ f'{i:.6f}' for i in flattenArray(r2[j].tolist()) ]
        checkAndPrint2(f'the values of  [{i} {j}]', A1, A2)
    else:
      raise ValueError('unepxectde')
  # DebugPush('print')
  assert len(y1) == len(y2)
  assert len(z1) == len(z2)
  for i in range(len(y1)):
    A1, A2 = list(flattenArray(y1[i].tolist())), list(flattenArray(y2[i].tolist()))
    assert len(A1) == len(A2)
    checkAndPrint(hprt(f'checking pCopy {i}', Shapes(p1=y1[i], p2=y2[i])))
    for j in range(len(A1)):
      checkAndPrint2(f'checking p[{i},{j}] {list(y1[i].shape)=} {y2[i].shape=}',
                     f'{A1[j]:.6f}', f'{A2[j]:.6f}') 

  for i in range(len(z1)):
    assert list(z1[i].shape) == z2[i].shape
    A1, A2 = list(flattenArray(z1[i].tolist())), list(flattenArray(z2[i].tolist()))
    assert len(A1) == len(A2)
    for j in range(len(A1)):
      checkAndPrint2(f'checking z[{i}],[{j}] / {len(z1)}',
                     f'{A1[j]:.6f}', f'{A2[j]:.6f}') 
    assert len(n1.parameters()), len(n2.parameters())
    assert len(n1.parameters()), len(m1)
    assert len(n1.parameters()), len(m2)
    # for pi in range(len(m1)):
    #   ii = pi%len(n1.parameters())
    #   p1 = n1.parameters()[ii]
    #   p2 = n2.parameters()[ii]
    #   q1 = m1[pi]
    #   q2 = m2[pi]
    #   checkAndPrint2(f'{pi}/{ii} checking shape of parameter {ii}', Shapes(p1=p1, p2=p2))
    #   checkAndPrint2(f'{pi}/{ii} checking shape of q1 and q2 {ii}', Shapes(q1=q1, q2=q2))
    assert len(n1.parameters()) == len(n2.parameters())
    for pi in range(len(n1.parameters())):
      p1 = n1.parameters()[pi]
      p2 = n2.parameters()[pi]
      checkAndPrint2(f'checking param {pi}', list(p1.shape), p2.shape)
      A1, A2 = list(flattenArray(p1.tolist())), list(flattenArray(p2.tolist()))
      assert len(A1) == len(A2)
      checkAndPrint(Shapes(p1=p1, p2=p2))
    for j in range(len(A1)):
      checkAndPrint2(f'checking param {pi}[{j}]',
                     f'{A1[j]:.6f}', f'{A2[j]:.6f}')

########################################################################
# BEGIN test calls
########################################################################

#### CALLS

TestArray()
testSimple()
testSanityChecks()
TestTensor()
backwardTest()
# TestHelpers()



# [[0 1 2 3 4 5]       \n
#  [6 7 8 9 10 11]     \n
#  [12 13 14 15 16 17]]')

if not CAP_TraditionalAssert:
  for i,k in enumerate(Failures):
    print(vprt(hprt(f'Fail #{i}'), vprt(k)))

  assert len(Failures) == 0, f'had {len(Failures)}/{TotalChecks} fails'
